import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {

  const [books, setBooks] = useState([]);
  var booksJson = [
    {
      "id": "1",
      "title": "Show Your Work",
      "authors": ["Austin Kleon"],
      "notes": "",
      "bookCategory": "Work",
      "bookCategoryImageName": "Category_1",
      "description": "",
      "thumbnail": ""
    },
    
    {
      "id": "2",
      "title": "Building a Story Brand",
      "authors": ["Donald Miller"],
      "notes": "",
      "bookCategory": "Market",
      "bookCategoryImageName": "Category_2",
      "description": "",
      "thumbnail": ""
    },
    
    {
      "id": "3",
      "title": "The Productivity Project",
      "authors": ["Chris Bailey"],
      "notes": "",
      "bookCategory": "Productive",
      "bookCategoryImageName": "Category_3",
      "description": "",
      "thumbnail": ""
    },
    
    {
      "id": "4",
      "title": "Profit First",
      "authors": ["Mike Michalowicz"],
      "notes": "",
      "bookCategory": "Money",
      "bookCategoryImageName": "Category_4",
      "description": "",
      "thumbnail": ""
    },
    
    {
      "id": "5",
      "title": "think and grow rich",
      "authors": ["napoleon hill"],
      "notes": "",
      "bookCategory": "Grow",
      "bookCategoryImageName": "Category_5",
      "description": "",
      "thumbnail": ""
    },
    
    {
      "id": "6",
      "title": "The 7 Habits of Highly Effective People",
      "authors": ["Stephen Covey"],
      "notes": "",
      "bookCategory": "Habits",
      "bookCategoryImageName": "Category_6",
      "description": "",
      "thumbnail": ""
    },
    
    {
      "id": "7",
      "title": "The ONE Thing",
      "authors": ["Gary Keller"],
      "notes": "",
      "bookCategory": "Focus",
      "bookCategoryImageName": "Category_7",
      "description": "",
      "thumbnail": ""
    }
    ] ;
  return (
    <div className="App">
         <h1>React HelloWorld App</h1>
         <p>
           <button onClick ={() =>setBooks(booksJson)}>
             fetch books
           </button>
         </p>
         <div>
        <table className="table table-striped" style={{ marginTop: 20 }}>
          <thead>
            <tr>
              <th>title</th>
              <th>bookCategory</th>
            </tr>
          </thead>
          <tbody>
          {books.map((book, index) => (
             <tr>
             <td>{book.title}</td>
             <td>{book.bookCategory}</td>
           </tr>
          ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default App;

