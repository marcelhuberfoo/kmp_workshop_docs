(function(root, factory) {
  if (typeof define === 'function' && define.amd) 
    define(['exports', 'kotlin', 'kotlinx-serialization-kotlinx-serialization-runtime', 'ktor-ktor-http', 'ktor-ktor-io', 'ktor-ktor-client-json'], factory);
  else if (typeof exports === 'object') 
    factory(module.exports, require('kotlin'), require('kotlinx-serialization-kotlinx-serialization-runtime'), require('ktor-ktor-http'), require('ktor-ktor-io'), require('ktor-ktor-client-json'));
  else {
    if (typeof kotlin === 'undefined') {
      throw new Error("Error loading module 'ktor-ktor-client-serialization'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'ktor-ktor-client-serialization'.");
    }
    if (typeof this['kotlinx-serialization-kotlinx-serialization-runtime'] === 'undefined') {
      throw new Error("Error loading module 'ktor-ktor-client-serialization'. Its dependency 'kotlinx-serialization-kotlinx-serialization-runtime' was not found. Please, check whether 'kotlinx-serialization-kotlinx-serialization-runtime' is loaded prior to 'ktor-ktor-client-serialization'.");
    }
    if (typeof this['ktor-ktor-http'] === 'undefined') {
      throw new Error("Error loading module 'ktor-ktor-client-serialization'. Its dependency 'ktor-ktor-http' was not found. Please, check whether 'ktor-ktor-http' is loaded prior to 'ktor-ktor-client-serialization'.");
    }
    if (typeof this['ktor-ktor-io'] === 'undefined') {
      throw new Error("Error loading module 'ktor-ktor-client-serialization'. Its dependency 'ktor-ktor-io' was not found. Please, check whether 'ktor-ktor-io' is loaded prior to 'ktor-ktor-client-serialization'.");
    }
    if (typeof this['ktor-ktor-client-json'] === 'undefined') {
      throw new Error("Error loading module 'ktor-ktor-client-serialization'. Its dependency 'ktor-ktor-client-json' was not found. Please, check whether 'ktor-ktor-client-json' is loaded prior to 'ktor-ktor-client-serialization'.");
    }
    root['ktor-ktor-client-serialization'] = factory(typeof this['ktor-ktor-client-serialization'] === 'undefined' ? {} : this['ktor-ktor-client-serialization'], kotlin, this['kotlinx-serialization-kotlinx-serialization-runtime'], this['ktor-ktor-http'], this['ktor-ktor-io'], this['ktor-ktor-client-json']);
  }
}(this, function(_, Kotlin, $module$kotlinx_serialization_kotlinx_serialization_runtime, $module$ktor_ktor_http, $module$ktor_ktor_io, $module$ktor_ktor_client_json) {
  'use strict';
  var defineInlineFunction = Kotlin.defineInlineFunction;
  var KSerializer = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.KSerializer;
  var throwCCE = Kotlin.throwCCE;
  var TextContent = $module$ktor_ktor_http.io.ktor.http.content.TextContent;
  var readText = $module$ktor_ktor_io.io.ktor.utils.io.core.readText_1lnizf$;
  var serializer = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.serializer_saj79j$;
  var serializer_0 = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.serializer_1yb8b7$;
  var ensureNotNull = Kotlin.ensureNotNull;
  var Json = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.json.Json;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var JsonSerializer = $module$ktor_ktor_client_json.io.ktor.client.features.json.JsonSerializer;
  var get_list = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.get_list_gekvwj$;
  var List = Kotlin.kotlin.collections.List;
  var firstOrNull = Kotlin.kotlin.collections.firstOrNull_us0mfu$;
  var kotlin_js_internal_StringCompanionObject = Kotlin.kotlin.js.internal.StringCompanionObject;
  var serializer_1 = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.serializer_6eet4j$;
  var get_set = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.get_set_gekvwj$;
  var Set = Kotlin.kotlin.collections.Set;
  var to = Kotlin.kotlin.to_ujzrz7$;
  var get_map = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.get_map_kgqhr1$;
  var Map = Kotlin.kotlin.collections.Map;
  var firstOrNull_0 = Kotlin.kotlin.collections.firstOrNull_7wnvza$;
  var json = $module$ktor_ktor_client_json.io.ktor.client.features.json;
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  function KotlinxSerializer(json) {
    if (json === void 0) 
      json = Json.Companion.plain;
    this.json_0 = json;
  }
  KotlinxSerializer.prototype.setMapper_cfhkba$ = function(type, serializer) {
};
  KotlinxSerializer.prototype.setListMapper_cfhkba$ = function(type, serializer) {
};
  KotlinxSerializer.prototype.register_ewacr1$ = defineInlineFunction('ktor-ktor-client-serialization.io.ktor.client.features.json.serializer.KotlinxSerializer.register_ewacr1$', function(T_0, isT, mapper) {
});
  KotlinxSerializer.prototype.registerList_ewacr1$ = defineInlineFunction('ktor-ktor-client-serialization.io.ktor.client.features.json.serializer.KotlinxSerializer.registerList_ewacr1$', function(T_0, isT, mapper) {
});
  KotlinxSerializer.prototype.register_30y1fr$ = defineInlineFunction('ktor-ktor-client-serialization.io.ktor.client.features.json.serializer.KotlinxSerializer.register_30y1fr$', function(T_0, isT) {
});
  KotlinxSerializer.prototype.registerList_30y1fr$ = defineInlineFunction('ktor-ktor-client-serialization.io.ktor.client.features.json.serializer.KotlinxSerializer.registerList_30y1fr$', function(T_0, isT) {
});
  KotlinxSerializer.prototype.write_ydd6c4$ = function(data, contentType) {
  var tmp$;
  var content = this.json_0.stringify_tf03ej$(Kotlin.isType(tmp$ = buildSerializer(data), KSerializer) ? tmp$ : throwCCE(), data);
  return new TextContent(content, contentType);
};
  KotlinxSerializer.prototype.read_2ktxo1$ = function(type, body) {
  var tmp$, tmp$_0;
  var text = readText(body);
  var mapper = (tmp$_0 = (tmp$ = type.kotlinType) != null ? serializer(tmp$) : null) != null ? tmp$_0 : serializer_0(type.type);
  return ensureNotNull(this.json_0.parse_awif5v$(mapper, text));
};
  KotlinxSerializer.$metadata$ = {
  kind: Kind_CLASS, 
  simpleName: 'KotlinxSerializer', 
  interfaces: [JsonSerializer]};
  function buildSerializer(value) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    if (Kotlin.isType(value, List)) 
      return get_list(elementSerializer(value));
    else if (Kotlin.isArray(value)) {
      return (tmp$_0 = (tmp$ = firstOrNull(value)) != null ? buildSerializer(tmp$) : null) != null ? tmp$_0 : get_list(serializer_1(kotlin_js_internal_StringCompanionObject));
    } else if (Kotlin.isType(value, Set)) 
      return get_set(elementSerializer(value));
    else if (Kotlin.isType(value, Map)) {
      var keySerializer = Kotlin.isType(tmp$_1 = elementSerializer(value.keys), KSerializer) ? tmp$_1 : throwCCE();
      var valueSerializer = Kotlin.isType(tmp$_2 = elementSerializer(value.values), KSerializer) ? tmp$_2 : throwCCE();
      return get_map(to(keySerializer, valueSerializer));
    } else 
      return serializer_0(Kotlin.getKClassFromExpression(value));
  }
  function elementSerializer($receiver) {
    var tmp$, tmp$_0;
    return (tmp$_0 = (tmp$ = firstOrNull_0($receiver)) != null ? buildSerializer(tmp$) : null) != null ? tmp$_0 : serializer_1(kotlin_js_internal_StringCompanionObject);
  }
  var initializer;
  function SerializerInitializer() {
    SerializerInitializer_instance = this;
    var $receiver = json.serializersStore;
    var element = new KotlinxSerializer();
    $receiver.add_11rb$(element);
  }
  SerializerInitializer.$metadata$ = {
  kind: Kind_OBJECT, 
  simpleName: 'SerializerInitializer', 
  interfaces: []};
  var SerializerInitializer_instance = null;
  function SerializerInitializer_getInstance() {
    if (SerializerInitializer_instance === null) {
      new SerializerInitializer();
    }
    return SerializerInitializer_instance;
  }
  var package$io = _.io || (_.io = {});
  var package$ktor = package$io.ktor || (package$io.ktor = {});
  var package$client = package$ktor.client || (package$ktor.client = {});
  var package$features = package$client.features || (package$client.features = {});
  var package$json = package$features.json || (package$features.json = {});
  var package$serializer = package$json.serializer || (package$json.serializer = {});
  package$serializer.KotlinxSerializer = KotlinxSerializer;
  Object.defineProperty(_, 'initializer', {
  get: function() {
  return initializer;
}});
  Object.defineProperty(_, 'SerializerInitializer', {
  get: SerializerInitializer_getInstance});
  KotlinxSerializer.prototype.write_za3rmp$ = JsonSerializer.prototype.write_za3rmp$;
  initializer = SerializerInitializer_getInstance();
  Kotlin.defineModule('ktor-ktor-client-serialization', _);
  return _;
}));
