(function (_, Kotlin, $module$ktor_ktor_http, $module$ktor_ktor_client_core, $module$ktor_ktor_client_json, $module$kotlinx_serialization_kotlinx_serialization_runtime, $module$ktor_ktor_client_serialization, $module$kotlinx_coroutines_core) {
  'use strict';
  var $$importsForInline$$ = _.$$importsForInline$$ || (_.$$importsForInline$$ = {});
  var takeFrom = $module$ktor_ktor_http.io.ktor.http.takeFrom_jl1sg7$;
  var Unit = Kotlin.kotlin.Unit;
  var COROUTINE_SUSPENDED = Kotlin.kotlin.coroutines.intrinsics.COROUTINE_SUSPENDED;
  var CoroutineImpl = Kotlin.kotlin.coroutines.CoroutineImpl;
  var List = Kotlin.kotlin.collections.List;
  var getKClass = Kotlin.getKClass;
  var createKType = Kotlin.createKType;
  var createInvariantKTypeProjection = Kotlin.createInvariantKTypeProjection;
  var PrimitiveClasses$stringClass = Kotlin.kotlin.reflect.js.internal.PrimitiveClasses.stringClass;
  var print = Kotlin.kotlin.io.print_s8jyv4$;
  var JsonFeature = $module$ktor_ktor_client_json.io.ktor.client.features.json.JsonFeature;
  var Json = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.json.Json;
  var KotlinxSerializer = $module$ktor_ktor_client_serialization.io.ktor.client.features.json.serializer.KotlinxSerializer;
  var HttpClient = $module$ktor_ktor_client_core.io.ktor.client.HttpClient_f0veat$;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var utils = $module$ktor_ktor_client_core.io.ktor.client.utils;
  var url = $module$ktor_ktor_client_core.io.ktor.client.request.url_3rzbk2$;
  var HttpMethod = $module$ktor_ktor_client_core.$$importsForInline$$['ktor-ktor-http'].io.ktor.http.HttpMethod;
  var HttpRequestBuilder_init = $module$ktor_ktor_client_core.io.ktor.client.request.HttpRequestBuilder;
  var HttpStatement_init = $module$ktor_ktor_client_core.io.ktor.client.statement.HttpStatement;
  var throwCCE = Kotlin.throwCCE;
  var equals = Kotlin.equals;
  var HttpResponse = $module$ktor_ktor_client_core.io.ktor.client.statement.HttpResponse;
  var complete = $module$ktor_ktor_client_core.io.ktor.client.statement.complete_abn2de$;
  var call = $module$ktor_ktor_client_core.io.ktor.client.call;
  var TypeInfo_init = $module$ktor_ktor_client_core.io.ktor.client.call.TypeInfo;
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  var SerialClassDescImpl = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.internal.SerialClassDescImpl;
  var internal = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.internal;
  var ArrayListSerializer = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.internal.ArrayListSerializer;
  var UnknownFieldException = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.UnknownFieldException;
  var GeneratedSerializer = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.internal.GeneratedSerializer;
  var MissingFieldException = $module$kotlinx_serialization_kotlinx_serialization_runtime.kotlinx.serialization.MissingFieldException;
  var coroutines = $module$kotlinx_coroutines_core.kotlinx.coroutines;
  var launch = $module$kotlinx_coroutines_core.kotlinx.coroutines.launch_s496o7$;
  var toString = Kotlin.toString;
  var Throwable = Error;
  var ArrayList_init = Kotlin.kotlin.collections.ArrayList_init_287e2$;
  function get$lambda($receiver) {
    return Unit;
  }
  function BookAPIService() {
    this.client_0 = HttpClient(BookAPIService$client$lambda);
    this.client2_0 = HttpClient(BookAPIService$client2$lambda);
  }
  function BookAPIService$apiUrl$lambda(closure$path) {
    return function ($receiver, it) {
      takeFrom($receiver, 'http://192.168.1.134:3010/');
      $receiver.encodedPath = closure$path;
      return Unit;
    };
  }
  BookAPIService.prototype.apiUrl_0 = function ($receiver, path) {
    $receiver.url_6yzzjr$(BookAPIService$apiUrl$lambda(path));
  };
  function BookAPIService$fetchBooks$lambda(this$BookAPIService) {
    return function ($receiver) {
      this$BookAPIService.apiUrl_0($receiver, 'books');
      return Unit;
    };
  }
  function Coroutine$fetchBooks($this, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.exceptionState_0 = 6;
    this.$this = $this;
    this.local$response = void 0;
  }
  Coroutine$fetchBooks.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: null,
    interfaces: [CoroutineImpl]
  };
  Coroutine$fetchBooks.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$fetchBooks.prototype.constructor = Coroutine$fetchBooks;
  Coroutine$fetchBooks.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            var $receiver_0 = this.$this.client_0;
            var host;
            var body;
            host = 'localhost';
            body = utils.EmptyContent;
            var $receiver_1 = new HttpRequestBuilder_init();
            url($receiver_1, 'http', host, 0, '/');
            $receiver_1.method = HttpMethod.Companion.Get;
            $receiver_1.body = body;
            BookAPIService$fetchBooks$lambda(this.$this)($receiver_1);
            var $this_0 = new HttpStatement_init($receiver_1, $receiver_0);
            var tmp$_3, tmp$_4, tmp$_5;
            tmp$_3 = createKType(getKClass(List), [createInvariantKTypeProjection(createKType(getKClass(Book), [], false))], false);
            if (equals(tmp$_3, createKType(getKClass(HttpStatement_init), [], false))) {
              this.result_0 = Kotlin.isType(tmp$_4 = $this_0, List) ? tmp$_4 : throwCCE();
              this.state_0 = 9;
              continue;
            }
             else {
              if (equals(tmp$_3, createKType(getKClass(HttpResponse), [], false))) {
                this.state_0 = 7;
                this.result_0 = $this_0.execute(this);
                if (this.result_0 === COROUTINE_SUSPENDED)
                  return COROUTINE_SUSPENDED;
                continue;
              }
               else {
                this.state_0 = 1;
                this.result_0 = $this_0.executeUnsafe(this);
                if (this.result_0 === COROUTINE_SUSPENDED)
                  return COROUTINE_SUSPENDED;
                continue;
              }
            }

          case 1:
            this.local$response = this.result_0;
            this.exceptionState_0 = 4;
            var tmp$_6;
            this.state_0 = 2;
            this.result_0 = this.local$response.call.receive_jo9acv$(new TypeInfo_init(getKClass(List), call.JsType, createKType(getKClass(List), [createInvariantKTypeProjection(createKType(getKClass(Book), [], false))], false)), this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 2:
            this.result_0 = Kotlin.isType(tmp$_6 = this.result_0, List) ? tmp$_6 : throwCCE();
            this.exceptionState_0 = 6;
            this.finallyPath_0 = [3];
            this.state_0 = 5;
            continue;
          case 3:
            this.state_0 = 8;
            continue;
          case 4:
            this.finallyPath_0 = [6];
            this.state_0 = 5;
            continue;
          case 5:
            this.exceptionState_0 = 6;
            complete(this.local$response);
            this.state_0 = this.finallyPath_0.shift();
            continue;
          case 6:
            throw this.exception_0;
          case 7:
            this.result_0 = Kotlin.isType(tmp$_5 = this.result_0, List) ? tmp$_5 : throwCCE();
            this.state_0 = 8;
            continue;
          case 8:
            this.state_0 = 9;
            continue;
          case 9:
            this.result_0;
            var jsonArrayString = this.result_0;
            return jsonArrayString;
          default:this.state_0 = 6;
            throw new Error('State Machine Unreachable execution');
        }
      }
       catch (e) {
        if (this.state_0 === 6) {
          this.exceptionState_0 = this.state_0;
          throw e;
        }
         else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  BookAPIService.prototype.fetchBooks = function (continuation_0, suspended) {
    var instance = new Coroutine$fetchBooks(this, continuation_0);
    if (suspended)
      return instance;
    else
      return instance.doResume(null);
  };
  function BookAPIService$fetchBooksAsString$lambda(this$BookAPIService) {
    return function ($receiver) {
      this$BookAPIService.apiUrl_0($receiver, 'books');
      return Unit;
    };
  }
  function Coroutine$fetchBooksAsString($this, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.exceptionState_0 = 6;
    this.$this = $this;
    this.local$response = void 0;
  }
  Coroutine$fetchBooksAsString.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: null,
    interfaces: [CoroutineImpl]
  };
  Coroutine$fetchBooksAsString.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$fetchBooksAsString.prototype.constructor = Coroutine$fetchBooksAsString;
  Coroutine$fetchBooksAsString.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            var $receiver_0 = this.$this.client2_0;
            var host;
            var body;
            host = 'localhost';
            body = utils.EmptyContent;
            var $receiver_1 = new HttpRequestBuilder_init();
            url($receiver_1, 'http', host, 0, '/');
            $receiver_1.method = HttpMethod.Companion.Get;
            $receiver_1.body = body;
            BookAPIService$fetchBooksAsString$lambda(this.$this)($receiver_1);
            var $this_0 = new HttpStatement_init($receiver_1, $receiver_0);
            var tmp$_3, tmp$_4, tmp$_5;
            tmp$_3 = createKType(PrimitiveClasses$stringClass, [], false);
            if (equals(tmp$_3, createKType(getKClass(HttpStatement_init), [], false))) {
              this.result_0 = typeof (tmp$_4 = $this_0) === 'string' ? tmp$_4 : throwCCE();
              this.state_0 = 9;
              continue;
            }
             else {
              if (equals(tmp$_3, createKType(getKClass(HttpResponse), [], false))) {
                this.state_0 = 7;
                this.result_0 = $this_0.execute(this);
                if (this.result_0 === COROUTINE_SUSPENDED)
                  return COROUTINE_SUSPENDED;
                continue;
              }
               else {
                this.state_0 = 1;
                this.result_0 = $this_0.executeUnsafe(this);
                if (this.result_0 === COROUTINE_SUSPENDED)
                  return COROUTINE_SUSPENDED;
                continue;
              }
            }

          case 1:
            this.local$response = this.result_0;
            this.exceptionState_0 = 4;
            var tmp$_6;
            this.state_0 = 2;
            this.result_0 = this.local$response.call.receive_jo9acv$(new TypeInfo_init(PrimitiveClasses$stringClass, call.JsType, createKType(PrimitiveClasses$stringClass, [], false)), this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 2:
            this.result_0 = typeof (tmp$_6 = this.result_0) === 'string' ? tmp$_6 : throwCCE();
            this.exceptionState_0 = 6;
            this.finallyPath_0 = [3];
            this.state_0 = 5;
            continue;
          case 3:
            this.state_0 = 8;
            continue;
          case 4:
            this.finallyPath_0 = [6];
            this.state_0 = 5;
            continue;
          case 5:
            this.exceptionState_0 = 6;
            complete(this.local$response);
            this.state_0 = this.finallyPath_0.shift();
            continue;
          case 6:
            throw this.exception_0;
          case 7:
            this.result_0 = typeof (tmp$_5 = this.result_0) === 'string' ? tmp$_5 : throwCCE();
            this.state_0 = 8;
            continue;
          case 8:
            this.state_0 = 9;
            continue;
          case 9:
            this.result_0;
            var resultString = this.result_0;
            print(resultString);
            return resultString;
          default:this.state_0 = 6;
            throw new Error('State Machine Unreachable execution');
        }
      }
       catch (e) {
        if (this.state_0 === 6) {
          this.exceptionState_0 = this.state_0;
          throw e;
        }
         else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  BookAPIService.prototype.fetchBooksAsString = function (continuation_0, suspended) {
    var instance = new Coroutine$fetchBooksAsString(this, continuation_0);
    if (suspended)
      return instance;
    else
      return instance.doResume(null);
  };
  function BookAPIService$client$lambda$lambda($receiver) {
    var $receiver_0 = new KotlinxSerializer(Json.Companion.nonstrict);
    $receiver_0.setMapper_cfhkba$(getKClass(Book), Book$Companion_getInstance().serializer());
    $receiver.serializer = $receiver_0;
    return Unit;
  }
  function BookAPIService$client$lambda($receiver) {
    $receiver.install_xlxg29$(JsonFeature.Feature, BookAPIService$client$lambda$lambda);
    return Unit;
  }
  function BookAPIService$client2$lambda($receiver) {
    return Unit;
  }
  BookAPIService.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'BookAPIService',
    interfaces: []
  };
  function Book(id, title, authors, notes, bookCategory, bookCategoryImageName, description, thumbnail) {
    Book$Companion_getInstance();
    this.id = id;
    this.title = title;
    this.authors = authors;
    this.notes = notes;
    this.bookCategory = bookCategory;
    this.bookCategoryImageName = bookCategoryImageName;
    this.description = description;
    this.thumbnail = thumbnail;
  }
  function Book$Companion() {
    Book$Companion_instance = this;
  }
  Book$Companion.prototype.serializer = function () {
    return Book$$serializer_getInstance();
  };
  Book$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var Book$Companion_instance = null;
  function Book$Companion_getInstance() {
    if (Book$Companion_instance === null) {
      new Book$Companion();
    }
    return Book$Companion_instance;
  }
  function Book$$serializer() {
    this.descriptor_2bxc5u$_0 = new SerialClassDescImpl('io.sunnyside.common.bookstoreadkmp.Book', this);
    this.descriptor.addElement_ivxn3r$('id', false);
    this.descriptor.addElement_ivxn3r$('title', false);
    this.descriptor.addElement_ivxn3r$('authors', false);
    this.descriptor.addElement_ivxn3r$('notes', false);
    this.descriptor.addElement_ivxn3r$('bookCategory', false);
    this.descriptor.addElement_ivxn3r$('bookCategoryImageName', false);
    this.descriptor.addElement_ivxn3r$('description', false);
    this.descriptor.addElement_ivxn3r$('thumbnail', false);
    Book$$serializer_instance = this;
  }
  Object.defineProperty(Book$$serializer.prototype, 'descriptor', {
    get: function () {
      return this.descriptor_2bxc5u$_0;
    }
  });
  Book$$serializer.prototype.serialize_awe97i$ = function (encoder, obj) {
    var output = encoder.beginStructure_r0sa6z$(this.descriptor, []);
    output.encodeStringElement_bgm7zs$(this.descriptor, 0, obj.id);
    output.encodeStringElement_bgm7zs$(this.descriptor, 1, obj.title);
    output.encodeSerializableElement_blecud$(this.descriptor, 2, new ArrayListSerializer(internal.StringSerializer), obj.authors);
    output.encodeStringElement_bgm7zs$(this.descriptor, 3, obj.notes);
    output.encodeStringElement_bgm7zs$(this.descriptor, 4, obj.bookCategory);
    output.encodeStringElement_bgm7zs$(this.descriptor, 5, obj.bookCategoryImageName);
    output.encodeStringElement_bgm7zs$(this.descriptor, 6, obj.description);
    output.encodeStringElement_bgm7zs$(this.descriptor, 7, obj.thumbnail);
    output.endStructure_qatsm0$(this.descriptor);
  };
  Book$$serializer.prototype.deserialize_nts5qn$ = function (decoder) {
    var index, readAll = false;
    var bitMask0 = 0;
    var local0
    , local1
    , local2
    , local3
    , local4
    , local5
    , local6
    , local7;
    var input = decoder.beginStructure_r0sa6z$(this.descriptor, []);
    loopLabel: while (true) {
      index = input.decodeElementIndex_qatsm0$(this.descriptor);
      switch (index) {
        case -2:
          readAll = true;
        case 0:
          local0 = input.decodeStringElement_3zr2iy$(this.descriptor, 0);
          bitMask0 |= 1;
          if (!readAll)
            break;
        case 1:
          local1 = input.decodeStringElement_3zr2iy$(this.descriptor, 1);
          bitMask0 |= 2;
          if (!readAll)
            break;
        case 2:
          local2 = (bitMask0 & 4) === 0 ? input.decodeSerializableElement_s44l7r$(this.descriptor, 2, new ArrayListSerializer(internal.StringSerializer)) : input.updateSerializableElement_ehubvl$(this.descriptor, 2, new ArrayListSerializer(internal.StringSerializer), local2);
          bitMask0 |= 4;
          if (!readAll)
            break;
        case 3:
          local3 = input.decodeStringElement_3zr2iy$(this.descriptor, 3);
          bitMask0 |= 8;
          if (!readAll)
            break;
        case 4:
          local4 = input.decodeStringElement_3zr2iy$(this.descriptor, 4);
          bitMask0 |= 16;
          if (!readAll)
            break;
        case 5:
          local5 = input.decodeStringElement_3zr2iy$(this.descriptor, 5);
          bitMask0 |= 32;
          if (!readAll)
            break;
        case 6:
          local6 = input.decodeStringElement_3zr2iy$(this.descriptor, 6);
          bitMask0 |= 64;
          if (!readAll)
            break;
        case 7:
          local7 = input.decodeStringElement_3zr2iy$(this.descriptor, 7);
          bitMask0 |= 128;
          if (!readAll)
            break;
        case -1:
          break loopLabel;
        default:throw new UnknownFieldException(index);
      }
    }
    input.endStructure_qatsm0$(this.descriptor);
    return Book_init(bitMask0, local0, local1, local2, local3, local4, local5, local6, local7, null);
  };
  Book$$serializer.prototype.childSerializers = function () {
    return [internal.StringSerializer, internal.StringSerializer, new ArrayListSerializer(internal.StringSerializer), internal.StringSerializer, internal.StringSerializer, internal.StringSerializer, internal.StringSerializer, internal.StringSerializer];
  };
  Book$$serializer.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: '$serializer',
    interfaces: [GeneratedSerializer]
  };
  var Book$$serializer_instance = null;
  function Book$$serializer_getInstance() {
    if (Book$$serializer_instance === null) {
      new Book$$serializer();
    }
    return Book$$serializer_instance;
  }
  function Book_init(seen1, id, title, authors, notes, bookCategory, bookCategoryImageName, description, thumbnail, serializationConstructorMarker) {
    var $this = serializationConstructorMarker || Object.create(Book.prototype);
    if ((seen1 & 1) === 0)
      throw new MissingFieldException('id');
    else
      $this.id = id;
    if ((seen1 & 2) === 0)
      throw new MissingFieldException('title');
    else
      $this.title = title;
    if ((seen1 & 4) === 0)
      throw new MissingFieldException('authors');
    else
      $this.authors = authors;
    if ((seen1 & 8) === 0)
      throw new MissingFieldException('notes');
    else
      $this.notes = notes;
    if ((seen1 & 16) === 0)
      throw new MissingFieldException('bookCategory');
    else
      $this.bookCategory = bookCategory;
    if ((seen1 & 32) === 0)
      throw new MissingFieldException('bookCategoryImageName');
    else
      $this.bookCategoryImageName = bookCategoryImageName;
    if ((seen1 & 64) === 0)
      throw new MissingFieldException('description');
    else
      $this.description = description;
    if ((seen1 & 128) === 0)
      throw new MissingFieldException('thumbnail');
    else
      $this.thumbnail = thumbnail;
    return $this;
  }
  Book.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Book',
    interfaces: []
  };
  Book.prototype.component1 = function () {
    return this.id;
  };
  Book.prototype.component2 = function () {
    return this.title;
  };
  Book.prototype.component3 = function () {
    return this.authors;
  };
  Book.prototype.component4 = function () {
    return this.notes;
  };
  Book.prototype.component5 = function () {
    return this.bookCategory;
  };
  Book.prototype.component6 = function () {
    return this.bookCategoryImageName;
  };
  Book.prototype.component7 = function () {
    return this.description;
  };
  Book.prototype.component8 = function () {
    return this.thumbnail;
  };
  Book.prototype.copy_6e40z5$ = function (id, title, authors, notes, bookCategory, bookCategoryImageName, description, thumbnail) {
    return new Book(id === void 0 ? this.id : id, title === void 0 ? this.title : title, authors === void 0 ? this.authors : authors, notes === void 0 ? this.notes : notes, bookCategory === void 0 ? this.bookCategory : bookCategory, bookCategoryImageName === void 0 ? this.bookCategoryImageName : bookCategoryImageName, description === void 0 ? this.description : description, thumbnail === void 0 ? this.thumbnail : thumbnail);
  };
  Book.prototype.toString = function () {
    return 'Book(id=' + Kotlin.toString(this.id) + (', title=' + Kotlin.toString(this.title)) + (', authors=' + Kotlin.toString(this.authors)) + (', notes=' + Kotlin.toString(this.notes)) + (', bookCategory=' + Kotlin.toString(this.bookCategory)) + (', bookCategoryImageName=' + Kotlin.toString(this.bookCategoryImageName)) + (', description=' + Kotlin.toString(this.description)) + (', thumbnail=' + Kotlin.toString(this.thumbnail)) + ')';
  };
  Book.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.id) | 0;
    result = result * 31 + Kotlin.hashCode(this.title) | 0;
    result = result * 31 + Kotlin.hashCode(this.authors) | 0;
    result = result * 31 + Kotlin.hashCode(this.notes) | 0;
    result = result * 31 + Kotlin.hashCode(this.bookCategory) | 0;
    result = result * 31 + Kotlin.hashCode(this.bookCategoryImageName) | 0;
    result = result * 31 + Kotlin.hashCode(this.description) | 0;
    result = result * 31 + Kotlin.hashCode(this.thumbnail) | 0;
    return result;
  };
  Book.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.id, other.id) && Kotlin.equals(this.title, other.title) && Kotlin.equals(this.authors, other.authors) && Kotlin.equals(this.notes, other.notes) && Kotlin.equals(this.bookCategory, other.bookCategory) && Kotlin.equals(this.bookCategoryImageName, other.bookCategoryImageName) && Kotlin.equals(this.description, other.description) && Kotlin.equals(this.thumbnail, other.thumbnail)))));
  };
  function BookRepository() {
    this.bookAPIService_0 = new BookAPIService();
  }
  function Coroutine$fetchBooksFromRepo($this, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.exceptionState_0 = 1;
    this.$this = $this;
  }
  Coroutine$fetchBooksFromRepo.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: null,
    interfaces: [CoroutineImpl]
  };
  Coroutine$fetchBooksFromRepo.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$fetchBooksFromRepo.prototype.constructor = Coroutine$fetchBooksFromRepo;
  Coroutine$fetchBooksFromRepo.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            this.state_0 = 2;
            this.result_0 = this.$this.bookAPIService_0.fetchBooks(this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 1:
            throw this.exception_0;
          case 2:
            var result = this.result_0;
            var bookList = ArrayList_init();
            var tmp$;
            tmp$ = result.iterator();
            while (tmp$.hasNext()) {
              var element = tmp$.next();
              bookList.add_11rb$(element);
            }

            return bookList;
          default:this.state_0 = 1;
            throw new Error('State Machine Unreachable execution');
        }
      }
       catch (e) {
        if (this.state_0 === 1) {
          this.exceptionState_0 = this.state_0;
          throw e;
        }
         else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  BookRepository.prototype.fetchBooksFromRepo = function (continuation_0, suspended) {
    var instance = new Coroutine$fetchBooksFromRepo(this, continuation_0);
    if (suspended)
      return instance;
    else
      return instance.doResume(null);
  };
  function Coroutine$fetchBooksFromRepoAsString($this, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.exceptionState_0 = 1;
    this.$this = $this;
  }
  Coroutine$fetchBooksFromRepoAsString.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: null,
    interfaces: [CoroutineImpl]
  };
  Coroutine$fetchBooksFromRepoAsString.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$fetchBooksFromRepoAsString.prototype.constructor = Coroutine$fetchBooksFromRepoAsString;
  Coroutine$fetchBooksFromRepoAsString.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            this.state_0 = 2;
            this.result_0 = this.$this.bookAPIService_0.fetchBooksAsString(this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 1:
            throw this.exception_0;
          case 2:
            var result = this.result_0;
            return result;
          default:this.state_0 = 1;
            throw new Error('State Machine Unreachable execution');
        }
      }
       catch (e) {
        if (this.state_0 === 1) {
          this.exceptionState_0 = this.state_0;
          throw e;
        }
         else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  BookRepository.prototype.fetchBooksFromRepoAsString = function (continuation_0, suspended) {
    var instance = new Coroutine$fetchBooksFromRepoAsString(this, continuation_0);
    if (suspended)
      return instance;
    else
      return instance.doResume(null);
  };
  function Coroutine$BookRepository$fetchBooksFromRepo$lambda(closure$success_0, this$BookRepository_0, $receiver_0, controller, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.$controller = controller;
    this.exceptionState_0 = 1;
    this.local$closure$success = closure$success_0;
    this.local$this$BookRepository = this$BookRepository_0;
  }
  Coroutine$BookRepository$fetchBooksFromRepo$lambda.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: null,
    interfaces: [CoroutineImpl]
  };
  Coroutine$BookRepository$fetchBooksFromRepo$lambda.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$BookRepository$fetchBooksFromRepo$lambda.prototype.constructor = Coroutine$BookRepository$fetchBooksFromRepo$lambda;
  Coroutine$BookRepository$fetchBooksFromRepo$lambda.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            this.state_0 = 2;
            this.result_0 = this.local$this$BookRepository.fetchBooksFromRepo(this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 1:
            throw this.exception_0;
          case 2:
            return this.local$closure$success(this.result_0);
          default:this.state_0 = 1;
            throw new Error('State Machine Unreachable execution');
        }
      }
       catch (e) {
        if (this.state_0 === 1) {
          this.exceptionState_0 = this.state_0;
          throw e;
        }
         else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  function BookRepository$fetchBooksFromRepo$lambda(closure$success_0, this$BookRepository_0) {
    return function ($receiver_0, continuation_0, suspended) {
      var instance = new Coroutine$BookRepository$fetchBooksFromRepo$lambda(closure$success_0, this$BookRepository_0, $receiver_0, this, continuation_0);
      if (suspended)
        return instance;
      else
        return instance.doResume(null);
    };
  }
  BookRepository.prototype.fetchBooksFromRepo_9ta5e1$ = function (success) {
    launch(coroutines.GlobalScope, coroutines.Dispatchers.Main, void 0, BookRepository$fetchBooksFromRepo$lambda(success, this));
  };
  BookRepository.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'BookRepository',
    interfaces: []
  };
  function Coroutine$fetchBooksFromRepoAsync$lambda(closure$success_0, closure$obj_0, closure$error_0, $receiver_0, controller, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.$controller = controller;
    this.exceptionState_0 = 5;
    this.local$closure$success = closure$success_0;
    this.local$closure$obj = closure$obj_0;
    this.local$closure$error = closure$error_0;
  }
  Coroutine$fetchBooksFromRepoAsync$lambda.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: null,
    interfaces: [CoroutineImpl]
  };
  Coroutine$fetchBooksFromRepoAsync$lambda.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$fetchBooksFromRepoAsync$lambda.prototype.constructor = Coroutine$fetchBooksFromRepoAsync$lambda;
  Coroutine$fetchBooksFromRepoAsync$lambda.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            this.exceptionState_0 = 2;
            this.state_0 = 1;
            this.result_0 = this.local$closure$obj.fetchBooksFromRepoAsString(this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 1:
            return this.local$closure$success(this.result_0);
          case 2:
            this.exceptionState_0 = 5;
            var e = this.exception_0;
            if (Kotlin.isType(e, Throwable)) {
              return this.local$closure$error('fetchBooksFromRepoAsync error:' + toString(e));
            }
             else {
              throw e;
            }

          case 3:
            this.state_0 = 4;
            continue;
          case 4:
            return;
          case 5:
            throw this.exception_0;
          default:this.state_0 = 5;
            throw new Error('State Machine Unreachable execution');
        }
      }
       catch (e) {
        if (this.state_0 === 5) {
          this.exceptionState_0 = this.state_0;
          throw e;
        }
         else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  function fetchBooksFromRepoAsync$lambda(closure$success_0, closure$obj_0, closure$error_0) {
    return function ($receiver_0, continuation_0, suspended) {
      var instance = new Coroutine$fetchBooksFromRepoAsync$lambda(closure$success_0, closure$obj_0, closure$error_0, $receiver_0, this, continuation_0);
      if (suspended)
        return instance;
      else
        return instance.doResume(null);
    };
  }
  function fetchBooksFromRepoAsync(obj, success, error) {
    launch(coroutines.GlobalScope, void 0, void 0, fetchBooksFromRepoAsync$lambda(success, obj, error));
  }
  $$importsForInline$$['ktor-ktor-client-core'] = $module$ktor_ktor_client_core;
  var package$io = _.io || (_.io = {});
  var package$sunnyside = package$io.sunnyside || (package$io.sunnyside = {});
  var package$common = package$sunnyside.common || (package$sunnyside.common = {});
  var package$bookstoreadkmp = package$common.bookstoreadkmp || (package$common.bookstoreadkmp = {});
  package$bookstoreadkmp.BookAPIService = BookAPIService;
  Object.defineProperty(Book, 'Companion', {
    get: Book$Companion_getInstance
  });
  Object.defineProperty(Book, '$serializer', {
    get: Book$$serializer_getInstance
  });
  package$bookstoreadkmp.Book_init_9n9b8x$ = Book_init;
  package$bookstoreadkmp.Book = Book;
  package$bookstoreadkmp.BookRepository = BookRepository;
  package$bookstoreadkmp.fetchBooksFromRepoAsync_7ll0vf$ = fetchBooksFromRepoAsync;
  Book$$serializer.prototype.patch_mynpiu$ = GeneratedSerializer.prototype.patch_mynpiu$;
  Kotlin.defineModule('BooksToReadKMP-shared', _);
  return _;
}(module.exports, require('kotlin'), require('ktor-ktor-http'), require('ktor-ktor-client-core'), require('ktor-ktor-client-json'), require('kotlinx-serialization-kotlinx-serialization-runtime'), require('ktor-ktor-client-serialization'), require('kotlinx-coroutines-core')));

//# sourceMappingURL=BooksToReadKMP-shared.js.map
