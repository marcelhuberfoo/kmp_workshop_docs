var config = {
  mode: 'development',
  resolve: {
    modules: [
      "node_modules"
    ]
  },
  plugins: [],
  module: {
    rules: []
  }
};

// entry
if (!config.entry) config.entry = [];
config.entry.push("/Users/mbenhajla/IdeaProjects/projecttemplateskmp_osx/02-FullStackApp/02-multiplatform/01-workTemplate/build/js/packages/BooksToReadKMP-shared/kotlin/BooksToReadKMP-shared.js");
config.output = {
    path: "/Users/mbenhajla/IdeaProjects/projecttemplateskmp_osx/02-FullStackApp/02-multiplatform/01-workTemplate/shared/build/distributions",
    filename: "shared.js"
};

// source maps
config.module.rules.push({
        test: /\.js$/,
        use: ["kotlin-source-map-loader"],
        enforce: "pre"
});
config.devtool = 'eval-source-map';

// save evaluated config file
var util = require('util');
var fs = require("fs");
var evaluatedConfig = util.inspect(config, {showHidden: false, depth: null, compact: false});
fs.writeFile("/Users/mbenhajla/IdeaProjects/projecttemplateskmp_osx/02-FullStackApp/02-multiplatform/01-workTemplate/shared/build/reports/webpack/BooksToReadKMP-shared/webpack.config.evaluated.js", evaluatedConfig, function (err) {});

// Report progress to console
// noinspection JSUnnecessarySemicolon
;(function(config) {
    const webpack = require('webpack');
    const handler = (percentage, message, ...args) => {
        let p = percentage * 100;
        let msg = `${Math.trunc(p / 10)}${Math.trunc(p % 10)}% ${message} ${args.join(' ')}`;
        msg = msg.replace(new RegExp("/Users/mbenhajla/IdeaProjects/projecttemplateskmp_osx/02-FullStackApp/02-multiplatform/01-workTemplate/build/js", 'g'), '');;
        console.log(msg);
    };

    config.plugins.push(new webpack.ProgressPlugin(handler))
})(config);
module.exports = config
