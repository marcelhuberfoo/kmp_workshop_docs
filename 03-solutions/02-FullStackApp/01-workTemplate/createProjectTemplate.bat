

rem ***************Folders ************************

rem create top level folders for android, ios and shared subprojects
mkdir  shared
mkdir  androidApp
mkdir  iosApp
mkdir  jsApp

rem ********** shared subproject *****************************

rem create  Sub-folders for the shared project
mkdir  shared\src\commonMain\kotlin
mkdir  shared\src\jvmMain\kotlin
mkdir  shared\src\iosMain\kotlin
mkdir  shared\src\jsMain\kotlin
mkdir  shared\src\main

rem  Create the source Files and the build gradle File

copy /y nul  shared\src\commonMain\kotlin\BookModel.kt
copy /y nul  shared\src\commonMain\kotlin\BookAPIService.kt
copy /y nul  shared\src\commonMain\kotlin\BookRepository.kt

copy /y nul  shared\src\jvmMain\kotlin\android.kt
copy /y nul  shared\src\iosMain\kotlin\ios.kt
copy /y nul  shared\src\jsMain\kotlin\js.kt
copy /y nul  shared\src\main\AndroidManifest.xml
copy /y nul  shared\build.gradle 


rem ********** iosApp subproject *****************************
rem  will be create with XCode IDE in the iosApp sub-directory



rem ********** androidApp subproject *****************************
rem  will be create with Android IDE in the androidApp sub-directory



rem ******** jsApp subproject *******************************

rem create  Sub-folders for the jsApp project
mkdir  jsApp\src\main\kotlin
mkdir  jsApp\src\main\resources
mkdir  jsApp\src\main\kotlin\io.sunnyside.bookstoread


rem  Create the source Files and the build gradle File

copy /y nul  jsApp\src\main\kotlin\io.sunnyside.bookstoread\main.kt
copy /y nul  jsApp\src\main\kotlin\io.sunnyside.bookstoread\ReactApp.kt
copy /y nul  jsApp\src\main\resources\index.html
copy /y nul  jsApp\src\main\resources\styles.css
copy /y nul  jsApp\build.gradle

